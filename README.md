# Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
*[#FFEBA1] Matematicas
	*[#FD6F96] Aplicaciones
		*_ es
			*[#FCFFA6] Regla que convierte los elementos\n de un determinado conjunto 
		*_ tipos
			*[#FCFFA6] inyectiva
				*[#FFFFFF] Si dos elementos tiene la misma imagen \nentonces son el mismo elemento
			*[#FCFFA6] sobreyectiva
				*[#FFFFFF] Si es igual
			*[#FCFFA6] biyectiva
				*[#FFFFFF] Cuando es Inyectiva y sobreyectiva
	*[#FD6F96] Transformación
		*[#FCFFA6] Se denomina aplicación o función
		*_ cuando
			*[#FCFFA6] Hay un proceso de cambio
	*[#FD6F96] Función
		*[#FCFFA6] Grafica de la función
			*[#95DAC1] Linea Recta 
			*[#95DAC1] Parábolas
			*[#95DAC1] Series de puntos
	*[#FD6F96] Conjuntos
		*[#FCFFA6] Se define como una colección de elementos
		*[#FCFFA6] Representación mediante Dibujos
			*[#95DAC1] Ayudan a resolver los problemas
			*_ denominados
				*[#95DAC1] Diagramas de Venn
					*[#FFFFFF] Es recursos didáctico
					*_ se representa
						*[#FFFFFF] Mediante círculos u óvalos
							*[#FFFFFF] Encierran los elementos\n que forman el conjunto
					*_ Aporte de
						*[#FFFFFF] John Venn 
					*_ cuando 
						*[#FFFFFF] 1880
					*[#FFFFFF] Ayudan a comprender \nintuitivamente la posición	
		*_ tipos
			*[#95DAC1] Universal
				*_ Donde
					*[#FFFFFF] En donde ocurre todas \nlas cosas de la teoría 
			*[#95DAC1] Vacio
				*_ cuando
					*[#FFFFFF] No tiene elemento  
		
		*[#FCFFA6] Cardinal de un conjunto.
			*_ es
				*[#FFFFFF] Número de elementos \nque conforman el conjunto

			*[#95DAC1] Según los elementos que tengan \ntienen una serie de propiedades	
				*_ como
					*[#FFFFFF] Acotación de cardinales
					*[#FFFFFF] Formula para resolver cardinales

		*[#FCFFA6] Inclusión de conjuntos
			*_ es
				*[#95DAC1] Cuando un conjunto está incluido en otro
				*[#95DAC1] Cuando todos los elementos \ndel primero pertenecen al segundo
		*[#FCFFA6] Operaciones con Conjuntos
			*_ es
				*[#95DAC1] Construir conjuntos\n más complejos
					*_ mediante 
						*[#ffffff] conjuntos elementales
			*_ son	
				*[#95DAC1] Intersección
					*[#FFFFFF] Elementos que pertenecen \nsimultáneamente a ambos
				*[#95DAC1] Unión
					*[#FFFFFF] Elementos que pertenecen \nal menos algunos de ellos
				*[#95DAC1] Complementación
					*[#FFFFFF] No pertenece a un conjunto dado	
			*[#95DAC1] Mezclar operaciones
				*_ resultan
					*[#FFFFFF] Diferencia de conjuntos	
			*[#95DAC1] Nacen propiedades
		*[#FCFFA6] Imagen de un subconjunto 
			*_ es
				*[#95DAC1] Transformación de manera \ngrafica mediante símbolos		
@endmindmap
```
# Funciones (2010)
```plantuml
@startmindmap
*[#E05D5D] Funciones
	*[#00A19D] Es
		*[#FFB344] Transformaciones de un \nconjunto en otro conjunto
		*_ ejemplo
			*[#ffffff] Cómo actuaba el sol \nen un muñeco de nieve 	
	*[#00A19D] Representaciones gráficas
		*[#FFB344] Representación cartesiana 
			*_ por 
				*[#ffffff] René Descartes
			*_ es
				*[#ffffff] Representar en un plano los \nvalores de los conjuntos de números
			*[#ffffff] La curva en la gráfica de la función
				*_ nos da 
					*[#ffffff] Cómo es la función
					*[#ffffff] Cómo cambia 	
	*[#00A19D] Características
		*[#FFB344] Continuidad
			*[#ffffff] Función que tiene buen comportamiento
			*[#ffffff] No produce sobresaltos\n ni discontinuidades 
			*[#ffffff] Son funciones más manejables

		*[#FFB344] Discontinuidad
			*[#ffffff] Si no es continua
			*[#ffffff] Presenta algún punto en \nel que existe un salto

		*[#FFB344] Limite
			*[#ffffff] Como se aproximan
			*[#ffffff] A qué valor se aproxima a \nlos valores de una función
		*[#FFB344] Intervalo
			*_ es
				*[#ffffff] Un trozo del rango de los \nvalores que se puede tomar
		*[#FFB344] Máximos y mínimos relativos 
			*_ son
				*[#ffffff]  valor mayor o menor que puede tomar\n en TODO su rango
		*[#FFB344] Dominio
			*[#ffffff] Conjunto de elementos x de \nla variable independiente		
	*[#00A19D] Tipos
		*_ divide
			*[#FFB344] Crecientes
				*_ es
					*[#ffffff] Cuando aumenta la variable independiente los\n valores del conjunto aumentan
			*[#FFB344] Decrecientes
				*_ es
					*[#ffffff] Cuando disminuye la variable independiente los\n valores del conjunto disminuyen
	*[#00A19D] Cálculo diferencial 
		*[#FFB344] Derivada
			*[#ffffff] Concepto muy importante 
			*[#ffffff] Resolver el problema de la aproximación 
				*_ como
					*[#ffffff]  Aproximar una función complicada\n con una función más simple
			*[#ffffff] Aparece en muchos sitios 
				*[#ffffff] economía
				*[#ffffff] Ciencias sociales 

@endmindmap
```
# La matemática del computador (2002)
```plantuml
@startmindmap
*[#E1E8EB] La matemática \ndel computador
	*[#FFB830] Matematicas
		*_ Es
			*[#FF616D] Ciencia que parte de\n una deducción lógica
				*_ permite 
					*[#FAFF00] Estudiar valores abstractos\n como los números 
						*[#FFFFFF] Sus características 
						*[#FFFFFF] Los vínculos existentes\n con otras áreas
		*[#FF616D] ES muy importante para el \ndesarrollo  de las computadoras
		*[#FF616D] Ayuda resolver los problemas cotidianos
		*[#FF616D] Representación interna de números
			*_ nos dice
				*[#FAFF00] Cómo convertir un paso o no\n de corriente de ceros y unos 
					*_ en 
						*[#ffffff] Una serie de posiciones de memoria
			*[#FAFF00] Representación interna\n de números enteros 
				*_ divide
					*[#ffffff] Representación en\n magnitud signo
					*[#ffffff] Representación \nen exceso
					*[#ffffff] Representación en\n complemento a 2

		*[#FF616D] Aritmética finita
			*[#FAFF00] Dígitos significativos
				*[#ffffff]  Miden la precisión general\n relativa de un valor
			*[#FAFF00] Truncar un número
				*_ es 
					*[#ffffff] Simplemente cortar un número \nque tiene una infinitos decimales
						*[#ffffff] PI
					*[#ffffff] Cortar unas cuantas cifras a la\n derecha a partir de una dada
					*[#ffffff] Es una aproximación 
			*[#FAFF00] Redondear un número
				*[#ffffff] Es una especie de \ntruncamiento refinado
				*[#ffffff] El error que se cometa\n  sea en media 
				*[#ffffff] Los errores cometidos\n sean lo menos posible.
			*[#FAFF00] Como se efectúan los\n cálculos en un computador


	*[#FFB830] Sistema que utiliza\n el computador
		*[#FF616D] Sistema binario
			*[#FAFF00] Lógica binaria de dos ordenadores
			*[#FAFF00] Enlaza con la lógica booleana
			*[#FAFF00] Representar un número mediante\n  bits o dígitos binarios
				*[#ffffff] Numero 0
					*[#ffffff] No paso de corriente 
				*[#ffffff] Numero 1
					*[#ffffff] Paso de corriente
			*[#FAFF00] Se pueden armar estructuras complejas 
				*[#ffffff] como 
					*[#ffffff] Letras
					*[#ffffff] Signos de puntuación 
					*[#ffffff] Imagenes
					*[#ffffff] Videos
		*[#FF616D] Sistema octal
			*[#FAFF00] Sistema de numeración de base 8
			*_ En informática 
				*[#ffffff] Se utiliza la numeración octal \nen vez de la hexadecimal
			*[#FAFF00]_ ventaja
				*[#ffffff] No requiere utilizar otros símbolos\n diferentes de los dígitos
		*[#FF616D] Sistema hexadecimal
			*[#FAFF00] Utiliza dieciséis dígitos	
		
@endmindmap
```
